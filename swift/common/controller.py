from __future__ import with_statement

import time
import os

from eventlet import tpool
from eventlet.timeout import Timeout
from webob.exc import HTTPServerError
from webob import Response
from webob.exc import HTTPServiceUnavailable, HTTPUnprocessableEntity

from swift.common.utils import get_logger, drop_buffer_cache, whataremyips
from swift.common.exceptions import ChunkWriteTimeout
from swift.common.http import is_success, HTTP_INTERNAL_SERVER_ERROR, \
    HTTP_SERVICE_UNAVAILABLE, HTTPClientDisconnect


def update_headers(response, headers):
    """
    Helper function to update headers in the response.

    :param response: webob.Response object
    :param headers: dictionary headers
    """
    if hasattr(headers, 'items'):
        headers = headers.items()
    for name, value in headers:
        if name == 'etag':
            response.headers[name] = value.replace('"', '')
        elif name not in ('date', 'content-length', 'content-type',
                          'connection', 'x-put-timestamp', 'x-delete-after'):
            response.headers[name] = value


class BaseController(object):
    """Base WSGI controller class"""
    server_type = _('Base')

    def __init__(self, conf, log_route):
        self.logger = get_logger(conf, log_route=log_route)
        self.   swift_dir = conf.get('swift_dir', '/etc/swift')
        self.error_suppression_interval = \
            int(conf.get('error_suppression_interval', 60))
        self.error_suppression_limit = \
            int(conf.get('error_suppression_limit', 10))
        self.port = int(conf.get('bind_port'))
        self.node_timeout = int(conf.get('node_timeout', 10))
        self.conn_timeout = float(conf.get('conn_timeout', 0.5))
        self.client_timeout = int(conf.get('client_timeout', 60))
        self.transfer_queue_depth = \
                    int(conf.get('put_transfer_queue_depth', 10))
        self.disk_queue_depth = \
                    int(conf.get('put_disk_queue_depth', 10))
        self.object_chunk_size = int(conf.get('object_chunk_size', 65536))

    def error_increment(self, node):
        """
        Handles incrementing error counts when talking to nodes.

        :param node: dictionary of node to increment the error count for
        """
        node['errors'] = node.get('errors', 0) + 1
        node['last_error'] = time.time()

    def error_occurred(self, node, msg):
        """
        Handle logging, and handling of errors.

        :param node: dictionary of node to handle errors for
        :param msg: error message
        """
        self.error_increment(node)
        self.logger.error(_('%(msg)s %(ip)s:%(port)s'),
            {'msg': msg, 'ip': node['ip'], 'port': node['port']})

    def exception_occurred(self, node, typ, additional_info):
        """
        Handle logging of generic exceptions.

        :param node: dictionary of node to log the error for
        :param typ: server type
        :param additional_info: additional information to log
        """
        self.logger.exception(
            _('ERROR with %(type)s server %(ip)s:%(port)s/%(device)s re: '
              '%(info)s'),
            {'type': typ, 'ip': node['ip'], 'port': node['port'],
             'device': node['device'], 'info': additional_info})

    def error_limited(self, node):
        """
        Check if the node is currently error limited.

        :param node: dictionary of node to check
        :returns: True if error limited, False otherwise
        """
        now = time.time()
        if not 'errors' in node:
            return False
        if 'last_error' in node and node['last_error'] < \
                now - self.error_suppression_interval:
            del node['last_error']
            if 'errors' in node:
                del node['errors']
            return False
        limited = node['errors'] > self.error_suppression_limit
        if limited:
            self.logger.debug(
                _('Node error limited %(ip)s:%(port)s (%(device)s)'), node)
        return limited

    def error_limit(self, node):
        """
        Mark a node as error limited.

        :param node: dictionary of node to error limit
        """
        node['errors'] = self.error_suppression_limit + 1
        node['last_error'] = time.time()

    def iter_nodes(self, partition, nodes, ring):
        """
        Node iterator that will first iterate over the normal nodes for a
        partition and then the handoff partitions for the node.

        :param partition: partition to iterate nodes for
        :param nodes: list of node dicts from the ring
        :param ring: ring to get handoff nodes from
        """
        for node in nodes:
            if not self.error_limited(node):
                yield node
        for node in ring.get_more_nodes(partition):
            if not self.error_limited(node):
                yield node

    def check_quorum(self, method, req, num_conns, min_quorum):
        """
        Check if there is a minimum quorum of connections

        :raise HTTPServiceUnavailable: in case there isn't a
        minimum quorum
        """
        if num_conns < min_quorum:
            self.logger.error(
                _('%(method)s exceptions during send, '
                  '%(conns)s/%(nodes)s required connections'),
                {'method': method, 'conns': num_conns, 'nodes': min_quorum})
            raise HTTPServiceUnavailable(request=req)

    def check_etag(self, req, etag):
        """
        Check if the request etag (if exists) is the same as the
        calculated etag

        :raise HTTPUnprocessableEntity: in case the provided etag
        is invalid
        """
        if 'etag' in req.headers and req.headers['etag'].lower() != etag:
            raise HTTPUnprocessableEntity(request=req)

    def check_uploaded_content_length(self, req, upload_size):
        """
        Check if the provided content-length is the same that was uploaded
        by the client

        :raise HTTPClientDisconnect: if the uploaded content length is invalid
        """
        content_length = None
        if req.content_length:
            content_length = req.content_length
        elif 'content-length' in req.headers:
            content_length = int(req.headers['content-length'])

        if content_length and content_length != upload_size:
                req.client_disconnect = True
                self.logger.warn(_('Client disconnected without'
                                   ' sending enough data. Content'
                                   'Length: %d. Upload size: %d' %
                                   (content_length, upload_size)))
                raise HTTPClientDisconnect(request=req)

    def check_stored_content_length(self, req, tmppath, file_size):
        """
        Check if the provided content-length is the same that was stored
        by the server

        :raise HTTPServerError: if the stored content length is invalid
        """
        content_length = None
        if req.content_length:
            content_length = req.content_length
        elif 'content-length' in req.headers:
            content_length = int(req.headers['content-length'])

        if content_length and content_length != file_size:
            self.logger.exception(_('ERROR: Temp file: %s is possibly '
                                    'corrupted. File size: %d. Content-length:'
                                    ' %d. Aborting PUT/STORE request: %s') %
                                      tmppath, file_size, content_length,
                                      req.path_info)
            raise HTTPServerError(body='Error while writing data to disk',
                                   request=req,
                                   content_type='text/plain')

    def remove_myself(self, nodes):
        """
        Check if this server is one of the nodes/devices where
        the object must be stored, and remove it from the
        node list if so and update the quorum
        """
        my_ips = whataremyips()
        devices = []
        for node in list(nodes):
            if node['ip'] in my_ips and \
                    node['port'] == self.port:
                devices.append(node['device'])
                nodes.remove(node)

        return nodes, devices

#    def remove_myself(self, device, nodes, quorum):
#        """
#        Check if this server is one of the nodes/devices where
#        the object must be stored, and remove it from the
#        node list if so and update the quorum
#        """
#        my_ips = whataremyips()
#        for node in list(nodes):
#            if node['ip'] in my_ips and \
#                  node['device'] == device:
#                quorum -= 1  # no need to connect to itself
#                nodes.remove(node)
#                break
#
#        return nodes, quorum

    def send_file(self, conn, path):
        """Method for a chunk PUT coro"""
        while True:
            chunk = conn.queue.get()
            if not conn.failed:
                try:
                    with ChunkWriteTimeout(self.node_timeout):
                        conn.send(chunk)
                except (Exception, ChunkWriteTimeout):
                    conn.failed = True
                    self.exception_occurred(conn.node, self.server_type,
                        _('Trying to write to %s') % path)
            conn.queue.task_done()

    def write_file(self, queue, fd, tmppath, path):
        """Method for writing data from a queue to disk"""
        upload_size = 0
        last_sync = 0
        while True:
            chunk = queue.get()
            upload_size += len(chunk)
            chunk = chunk[:]
            try:
                while chunk:
                    written = os.write(fd, chunk)
                    chunk = chunk[written:]
                # For large chunks sync every 512MB (by default) written
                if upload_size - last_sync >= self.bytes_per_sync:
                    tpool.execute(os.fdatasync, fd)
                    drop_buffer_cache(fd, last_sync, upload_size - last_sync)
                    last_sync = upload_size
            except Exception, exc:
                self.logger.exception(_('ERROR while trying to write to temp'
                                        ' file: %s. Request path: %s') %
                                      tmppath, path)
                return exc
            queue.task_done()

    def collect_responses(self, req, conns, method):
        """
        Collects final responses from open connections
        """
        responses = []
        for conn in conns:
            try:
                with Timeout(self.node_timeout):
                    response = conn.getresponse()
                    response.node = conn.node
                    responses.append(response)
            except (Exception, Timeout):
                self.exception_occurred(conn.node, self.server_type,
                    _('Trying to get final status of %s to %s') % (method,
                                                                req.path))
        return responses

    def extract_responses(self, req, responses):
        """
        Extracts attributes from a list of responses
        and returns a tuple containing lists of attributes
        and a set of etags extracted from these responses

        :param req: webob.Request object
        :param responses: list of eventlet.green.httplib.HTTPResponse
                                (response from other servers)
        """
        statuses = []
        reasons = []
        bodies = []
        etags = set()

        for response in responses:
            statuses.append(response.status)
            reasons.append(response.reason)
            bodies.append(response.read())
            if response.status >= HTTP_INTERNAL_SERVER_ERROR:
                self.error_occurred(response.node,
                    _('ERROR %(status)d %(body)s From %(type)s'
                      ' Server re: %(path)s') % {'status':
                        response.status, 'body': bodies[-1][:1024],
                      'path': req.path, 'type': self.server_type})
            elif is_success(response.status):
                etags.add(response.getheader('etag').strip('"'))

        return statuses, reasons, bodies, etags

    def generate_response(self, req, local_response, other_responses,
                            replicas):
        """
        Collects responses from remote servers and from the local server
        and chooses the best to return to the API

        :param req: webob.Request object
        :param local_response: Webob.Response reseponse from this server
        :param other_responses: list of eventlet.green.httplib.HTTPResponse
                                (response from other servers)
        :param replicas replication factor for this server
        """

        # extract attributes from remote responses
        # (eventlet.green.httplib.HTTPResponse)
        statuses, reasons, bodies, etags = self.extract_responses(
                                                req, other_responses)

        # extract attributes from local response (webob.Response)
        if local_response:
            statuses.append(local_response.code)
            reasons.append(local_response.title)
            bodies.append(local_response.status)
            if local_response.etag:
                etags.add(local_response.etag)

        if len(etags) > 1:
            self.logger.error(
                _('%s servers returned %s mismatched etags'), self.server_type,
                len(etags))
            raise HTTPServerError(request=req)

        while len(statuses) < replicas:
            statuses.append(HTTP_SERVICE_UNAVAILABLE)
            reasons.append('')
            bodies.append('')

        etag = len(etags) and etags.pop() or None

        resp = self.best_response(req, statuses, reasons, bodies, etag=etag)
        resp.last_modified = float(req.headers['X-Timestamp'])

        return resp

    def best_response(self, req, statuses, reasons, bodies, etag=None):
        """
        Given a list of responses from several servers, choose the best to
        return to the API.

        :param req: webob.Request object
        :param statuses: list of statuses returned
        :param reasons: list of reasons for each status
        :param bodies: bodies of each response
        :param server_type: type of server the responses came from
        :param etag: etag
        :returns: webob.Response object with the correct status, body, etc. set
        """
        resp = Response(request=req)
        if len(statuses):
            for hundred in (200, 300, 400):
                hstatuses = \
                    [s for s in statuses if hundred <= s < hundred + 100]
                if len(hstatuses) > len(statuses) / 2:
                    status = max(hstatuses)
                    status_index = statuses.index(status)
                    resp.status = '%s %s' % (status, reasons[status_index])
                    resp.body = bodies[status_index]
                    resp.content_type = 'text/html'
                    if etag:
                        resp.headers['etag'] = etag.strip('"')
                    return resp
        self.logger.error(_('%(type)s returning 503 for %(statuses)s'),
                              {'type': self.server_type, 'statuses': statuses})
        resp.status = '503 Internal Server Error'
        return resp
