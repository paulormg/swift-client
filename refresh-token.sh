#!/bin/bash

PROXY_HOST=localhost
TMP_FILE=/tmp/$RANDOM
AUTH_TOKEN=.authtoken
STORAGE_URL=.storageurl

curl -v -H 'X-Storage-User: test:tester' -H 'X-Storage-Pass: testing' http://$PROXY_HOST:8080/auth/v1.0 >/dev/null 2>$TMP_FILE

cat $TMP_FILE | awk '/X-Auth-Token/ { sub(/\r$/,""); print $3 }' > $AUTH_TOKEN
cat $TMP_FILE | awk '/X-Storage-Url/ { sub(/\r$/,""); print $3 }' > $STORAGE_URL

#echo $TMP_FILE

#rm -rf $TMP_FILE
