#!/usr/bin/python

import datetime
import sys
import cPickle as pickle

# CONSTANTS
PARAM_ACTION = 'action'
PARAM_OBJNAME = 'obj_name'
PARAM_LOCALPATH = 'local_path'
PARAM_BLOCKSIZE = 'block_size'
PARAM_NP = 'np'
PARAM_DEDUP = 'dedup'
PARAM_INLINE_SERVER = 'server-inline'
PARAM_WHOLE_OBJECT = 'whole-object'

ACTION_PUT = 'put'
ACTION_GET = 'get'


def log(msg):
    print "[%s] %s" % (str(datetime.datetime.now()), msg)
    sys.stdout.flush()


def write_stats(stats_file, results):
    with open(stats_file, "w") as f:
        pickle.dump(results, f)
