#!/usr/bin/python

from client import *

import time
import pickle
import sqlite3
import binascii
import hashlib
import zlib
import pprint
import os

PARAM_CATALOG='catalog_dir'
PARAM_FILESDIR='files_dir'
RENEW_TOKEN=30000

def parse_args():
    parser = argparse.ArgumentParser(description='Openstack Object Storage Client')
    parser.add_argument(PARAM_ACTION, type=str, choices=[ACTION_PUT, ACTION_GET],
                        help='the action to execute')
    parser.add_argument(PARAM_CATALOG, type=str, help='catalog directory')
    parser.add_argument(PARAM_FILESDIR, type=str, help='release files directory')
    parser.add_argument('--no-dedup', dest='dedup', action='store_false',
                        help='Do not chunk objects and perform deduplication')
    parser.add_argument('--inline-server', dest=PARAM_INLINE_SERVER, action='store_true',
                        help='Perform fingerprinting at server')
    args = parser.parse_args()

    return vars(args)

def create_container(container):
    os.system('bash /home/pgomes/swift/swift-client/request.sh PUT ' + container)

def renew_token():
    os.system('bash /home/pgomes/swift/swift-client/refresh-token.sh')

def main(params):
    catalog_dir = params[PARAM_CATALOG]
    files_dir = params[PARAM_FILESDIR]
    params[PARAM_WHOLE_OBJECT] = True

    package = catalog_dir.split('/')[-1]
    client = Client(package, params, 2)
   
    client.log("Starting upload of catalog" + package)

    renew_token()
    create_container(package)

    with open(catalog_dir) as f:
        catalog = pickle.load(f)    

    counter = 1
    for obj_name, path in catalog:
        counter += 1
        path = "%s/%s" % (files_dir, path)

        client.go(obj_name, path)	
       
        if counter % RENEW_TOKEN == 0:
            client.log("renewing token")
            renew_token()
        elif counter == 5000:
            break
        
        #print "%s to %s" % ( path, obj_name)

    client.pool.wait_completion()
    ts = time.time()
    logname = 'log-cernvm-' + package
    logname += ('-dedup' if params['dedup'] else '-non-dedup')
    logname += ('-server-' if params[PARAM_INLINE_SERVER] else '-client-')
    logname += str(ts)
    #print logname
    log_file = '/home/pgomes/swift/swift-client/stats/' + logname  
    client.write_stats(log_file)
    client.log("finished. wrote stats to: " + log_file)
    #client.print_stats()

    client.close_conn()

if __name__ == "__main__":

    args = parse_args()
    main(args)
