AUTH_TOKEN=`cat .authtoken`
STORAGE_URL=`cat .storageurl`
REQ_TYPE=$1
CONTAINER=$2

curl -v -X $REQ_TYPE -H "X-Auth-Token: $AUTH_TOKEN" $STORAGE_URL/$CONTAINER > /dev/null 2>&1
