#!/usr/bin/python

from common import PARAM_ACTION, ACTION_PUT, ACTION_GET, PARAM_INLINE_SERVER, \
    PARAM_DEDUP, PARAM_NP, write_stats
import argparse
import cPickle as pickle
import datetime
import multiprocessing
import os
import sys
import time
import workers

PARAM_CATALOG = 'catalog_dir'
PARAM_FILESDIR = 'files_dir'
RENEW_TOKEN = 30000


def parse_args():
    parser = argparse.ArgumentParser(description='Openstack Object Storage Client')
    parser.add_argument(PARAM_ACTION, type=str, choices=[ACTION_PUT, ACTION_GET],
                        help='the action to execute')
    parser.add_argument(PARAM_CATALOG, type=str, help='catalog directory')
    parser.add_argument('-np',dest=PARAM_NP, default=1, type=int, help='num procs')
    parser.add_argument(PARAM_FILESDIR, type=str, help='release files directory')
    parser.add_argument('--no-dedup', dest='dedup', action='store_false',
                        help='Do not chunk objects and perform deduplication')
    parser.add_argument('--inline-server', dest=PARAM_INLINE_SERVER, action='store_true',
                        help='Perform fingerprinting at server')
    args = parser.parse_args()

    return vars(args)


def create_container(container):
    os.system('bash /home/pgomes/swift/swift-client/request.sh PUT '
              + container)


def renew_token():
    os.system('bash /home/pgomes/swift/swift-client/refresh-token.sh')


def get_token():
    try:
        with open(".authtoken") as f:
            return f.read().strip()
    except Exception:
        sys.exit("ERROR: Please run ./refresh-token.sh before using "
                 "the client.")


def log(msg):
    print "[%s] %s" % (str(datetime.datetime.now()), msg)
    sys.stdout.flush()


def get_worker_type(dedup, action_name):
    workertype = None
    if dedup:
        if action_name == ACTION_GET:
            workertype = workers.WholeFileGet
        else:
            workertype = workers.WholeFilePut
    else:
        if action_name == ACTION_GET:
            workertype = workers.NonDedupGet
        else:
            workertype = workers.NonDedupPut
    return workertype


def main(params):
    catalog_dir = params[PARAM_CATALOG]
    files_dir = params[PARAM_FILESDIR]
    #inline = params[PARAM_INLINE_SERVER]
    np = params[PARAM_NP]
    dedup = params[PARAM_DEDUP]
    action_name = params[PARAM_ACTION]

    package = catalog_dir.split('/')[-1]
    #client = Client(package, params, 2)

    #log("Starting upload of catalog" + package)

    renew_token()
    token = get_token()
    create_container(package)

    with open(catalog_dir) as f:
        catalog = pickle.load(f)

    work_queue = multiprocessing.JoinableQueue(50)
    result_queue = multiprocessing.JoinableQueue()

    wrks = []
    # spawn workers
    for i in range(np):
        worker_type = get_worker_type(dedup, action_name)
        worker = worker_type(work_queue, result_queue, package)
        worker.start()
        wrks.append(worker)
    a, b = multiprocessing.Pipe()
    logger_worker = workers.LoggerWorker(result_queue, b)
    logger_worker.start()

    t1 = time.time()
    counter = 1
    for obj_name, path in catalog:
        counter += 1
        path = "%s/%s" % (files_dir, path)

        job = (counter, obj_name, path, token)
        work_queue.put(job)

        if counter % RENEW_TOKEN == 0:
            log("renewing token")
            renew_token()
            token = get_token()
        #elif counter == 2000:
        #    break

        #print "%s to %s" % ( path, obj_name)
    total = time.time() - t1

    log("Waiting queue to be consumed.")
    work_queue.join()
    ts = time.time()
    for worker in wrks:
        worker.terminate()
    log("Finished. Saving results.")

    times = a.recv()
    logname = 'log-cernvm-' + action_name + '-' + package
    logname += ('-dedup' if params['dedup'] else '-non-dedup')
    logname += ('-server-' if params[PARAM_INLINE_SERVER] else '-client-')
    logname += str(ts)
    log_file = '/home/pgomes/swift/swift-client/stats/' + logname
    results = {}
    results['total'] = total
    results['times'] = times
    write_stats(log_file, results)
    log("finished. wrote stats to: " + log_file)
    logger_worker.terminate()


if __name__ == "__main__":

    args = parse_args()
    main(args)
