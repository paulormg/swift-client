#!/usr/bin/python

import time
import pickle
import sqlite3
import binascii
import hashlib
import zlib
import pprint
import os
import sys
import random

def main():
    if len(sys.argv) != 2:
	exit("error - specify catalog dir")

    catalog_dir = sys.argv[1]
    package = catalog_dir.split('/')[-1]
    
    conn = sqlite3.connect(catalog_dir)
    cursor = conn.cursor()
    cursor.execute("select md5path_1, md5path_2, name, hash from catalog where flags=4")

    catalogs = []
    for row in cursor:
        md51 = row[0] & 0xffffffffffffffff
        md52 = row[1] & 0xffffffffffffffff
        md53 =  ((md51 << 64) | md52)
	md5hex = "%.32x" % md53
        name = row[2]
        obj_name = md5hex + "-" + str(name)
        sha1 = binascii.hexlify(row[3])
        path = "%s/%s" % (sha1[:2], sha1[2:])

        catalogs.append((obj_name,path))

    random.shuffle(catalogs)
    with open('/home/pgomes/swift/swift-client/catalogs/' + package + '.pkl','w') as f:
        pickle.dump(catalogs, f)

if __name__ == "__main__":

    main()
