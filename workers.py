from swift.common.ring.ring import Ring
import Queue
import cStringIO
import hashlib
import json
import multiprocessing
import os
import pycurl
import random
import tempfile
import time
import urllib

SERVER_URL = "127.0.0.1:8080"
CHUNK_BASE = '/chunk/v1/a/c/o/'
OBJ_BASE = '/v1/AUTH_test/'


def dummy_write(data):
    #print data
    pass


class LoggerWorker(multiprocessing.Process):

    def __init__(self, result_queue, conn):
        # base class initialization
        multiprocessing.Process.__init__(self)
        # job management stuff
        self.result_list = []
        self.result_queue = result_queue
        self.conn = conn

    def run(self):
        while True:
            try:
                job = self.result_queue.get(timeout=10)
            except Queue.Empty:
                self.conn.send(self.result_list)
                break
            self.result_list.append(job)
            #print job
            self.result_queue.task_done()


class Worker(multiprocessing.Process):

    def __init__(self, work_queue, result_queue, container):
        # base class initialization
        multiprocessing.Process.__init__(self)
        self.objbase = SERVER_URL + OBJ_BASE + container + '/'
        # job management stuff
        self.work_queue = work_queue
        self.result_queue = result_queue
        self.conn = pycurl.Curl()
        self.conn.setopt(pycurl.FAILONERROR, True)
        self.conn.setopt(pycurl.NOPROGRESS, 1)

    def run(self):
        while True:
            job = self.work_queue.get()
            result = self.do_job(job)
            # store the result
            if result != None:
                self.result_queue.put(result)
            self.work_queue.task_done()

    def do_job(self, job):
        pass

    def obj_url(self, obj_name):
        return self.objbase + obj_name

    def chunk_url(self, fingerprint):
        return SERVER_URL + CHUNK_BASE + fingerprint

    def http_put(self, url, data, length, headers, auth_token=None):
        #if header_function:
        #    self.conn.setopt(pycurl.HEADERFUNCTION, header_function)

        self.conn.setopt(pycurl.WRITEFUNCTION, dummy_write)
        self.conn.setopt(pycurl.UPLOAD, 1)
        self.conn.setopt(pycurl.INFILESIZE, length)
        if not data:
            headers.append('Expect:')
        else:
            if isinstance(data, file):
                self.conn.setopt(pycurl.INFILE, data)
            elif isinstance(data, str):
                data = cStringIO.StringIO(data)
                self.conn.setopt(pycurl.READFUNCTION, data.read)
            else:
                self.conn.setopt(pycurl.READFUNCTION, data)

        self.http_req(url, headers, auth_token)

    def http_get(self, url, headers, write_function, auth_token=None):
        self.conn.setopt(pycurl.WRITEFUNCTION, write_function)
        self.conn.setopt(pycurl.UPLOAD, 0)
        self.http_req(url, headers, auth_token)

    def http_req(self, url, headers, auth_token=None):

        if auth_token:
            headers.append('X-Auth-Token: ' + str(auth_token))

        split = url.split('/', 1)
        url = split[0] + '/' + urllib.quote(split[1])

        self.conn.setopt(pycurl.URL, url)
        self.conn.setopt(pycurl.HTTPHEADER, headers)
        self.perform()

    def perform(self):
        i = 0
        while True:
            try:
                self.conn.perform()
                break
            except Exception, exc:
                if i < 2:
                    i += 1
                    print "Error on request. Retrying." \
                          "Count: %d" % (i)
                else:
                    print "Error on request. " \
                          "Already tried %d times. Exiting." % (i)
                    raise exc


class PersistentAndLoggerWorker(multiprocessing.Process):

    def __init__(self, result_queue, conn, numchunks, filename):
        # base class initialization
        multiprocessing.Process.__init__(self)
        # job management stuff
        self.result_list = []
        self.result_queue = result_queue
        self.conn = conn
        self.numchunks = numchunks
        self.filename = filename
        self.currentchunk = 0
        self.received = {}

    def run(self):
        with open(self.filename, 'w') as f:
            while self.currentchunk < self.numchunks:
                job = self.result_queue.get()
                i, chunk, t_total = job
                self.received[i] = chunk
                self.result_list.append((i, t_total))
                while self.currentchunk in self.received:
                    f.write(self.received[self.currentchunk])
                    del self.received[self.currentchunk]
                    self.currentchunk += 1
                self.result_queue.task_done()
                #print "currChunk: %d numChunks: %d" % (self.currentchunk, self.numchunks)
        self.conn.send(self.result_list)


class ManifestAndLoggerWorker(multiprocessing.Process):

    def __init__(self, result_queue, conn, numchunks):
        # base class initialization
        multiprocessing.Process.__init__(self)
        # job management stuff
        self.result_list = []
        self.result_queue = result_queue
        self.conn = conn
        self.numchunks = numchunks
        self.chunklist = []
        self.currentchunk = 0
        self.received = {}

    def run(self):
        while self.currentchunk < self.numchunks:
            job = self.result_queue.get()
            t_total, t_finger, i, chunk_meta = job
            self.result_list.append((i, t_total, t_finger))
            self.received[i] = chunk_meta
            self.result_queue.task_done()
            while self.currentchunk in self.received:
                self.chunklist.append(self.received[self.currentchunk])
                del self.received[self.currentchunk]
                self.currentchunk += 1
        self.conn.send((self.chunklist, self.result_list))

class DedupWorker(Worker):
    def __init__(self, work_queue, result_queue, container):
        Worker.__init__(self, work_queue, result_queue, container)

    def put_manifest(self, obj_name, manifest, authtoken):
        url = self.obj_url(obj_name)
        manifest = manifest.encode('utf-8')

        headers = []
        headers.append("Content-type: manifest/json")

        self.http_put(url, manifest, len(manifest), headers, authtoken)

    def get_manifest(self, obj_name, auth_token):
        url = self.obj_url(obj_name)
        buf = cStringIO.StringIO()
        self.http_get(url, [], buf.write, auth_token=auth_token)

        manifest = buf.getvalue()
        return (dict(json.loads(manifest)), len(manifest))

    def get_chunks(self, manifest, dst_path):
        #print "Getting chunks"
        chunks = manifest['chunks']
        with tempfile.TemporaryFile() as f:
            for chunk in chunks:
                fingerprint = chunk['id'].strip().encode('utf-8')
                url = self.chunk_url(fingerprint)
                self.http_get(url, [], f.write)

    def calculate_objid_hash(self, objid):
        return hashlib.md5(objid).hexdigest()

    def calculate_fingerprint(self, chunk):
        return hashlib.sha1(chunk).hexdigest()



class GetChunkWorker(DedupWorker):
    def __init__(self, work_queue, result_queue, direct):
        DedupWorker.__init__(self, work_queue, result_queue, '')
        self.direct = direct
        if self.direct:
            self.ring = Ring("/home/pgomes/system/etc/swift/",
                             ring_name='chunk')

    def do_job(self, job):
        i, fingerprint = job
        t = time.time()
        buf = cStringIO.StringIO()

        if self.direct:
            partition, nodes = self.ring.get_chunk_nodes(fingerprint)
            random.shuffle(nodes)
            node = nodes[0]
            #self.conn.setopt(pycurl.VERBOSE, True)
            url = node['ip'] + ":" + str(node['port']) + \
                  "/" + node['device'] + "/" + str(partition) + \
                  "/a/c/o/" + fingerprint
            #self.conn.setopt(pycurl.CUSTOMREQUEST, 'DEDUP_PUT')
        else:
            url = self.chunk_url(fingerprint)
 
        self.http_get(url, [], buf.write)
        chunk = buf.getvalue()
        t_total = time.time() - t
        return (i, chunk, t_total)


class PutChunkWorker(DedupWorker):
    def __init__(self, work_queue, result_queue, obj_hash, direct=False):
        self.obj_hash = obj_hash
        self.direct = direct
        if self.direct:
            self.ring = Ring("/home/pgomes/system/etc/swift/",
                             ring_name='chunk')
        DedupWorker.__init__(self, work_queue, result_queue, '')

    def do_job(self, job):
        i, chunk = job
        t = time.time()
        length = len(chunk)
        t_finger = time.time()
        fingerprint = self.calculate_fingerprint(chunk)
        t_finger = time.time() - t_finger

        if self.direct:
            partition, nodes = self.ring.get_chunk_nodes(fingerprint)
            random.shuffle(nodes)
            node = nodes[0]
            #self.conn.setopt(pycurl.VERBOSE, True)
            url = node['ip'] + ":" + str(node['port']) + \
                  "/" + node['device'] + "/" + str(partition) + \
                  "/a/c/o/" + fingerprint
            #self.conn.setopt(pycurl.CUSTOMREQUEST, 'DEDUP_PUT')
        else:
            url = self.chunk_url(fingerprint)
        headers = []
        headers.append("Content-type: application/octet-stream")
        headers.append("X-Chunk-Obj-Id: " + self.obj_hash)
        self.http_put(url, chunk, length, headers)
        chunk_meta = {}
        chunk_meta['id'] = fingerprint
        t_total = time.time() - t
        return (t_total, t_finger, i, chunk_meta)


class NonDedupPut(Worker):

    def __init__(self, work_queue, result_queue, container):
        Worker.__init__(self, work_queue, result_queue, container)

    def do_job(self, job):
        i, obj_name, src_path, authtoken = job
        t = time.time()
        headers = []
        headers.append("Content-type: application/octet-stream")
        url = self.obj_url(obj_name)
        with open(src_path) as f:
            fsize = os.fstat(f.fileno()).st_size
            self.http_put(url, f, fsize, headers, authtoken)
        return (i, time.time() - t, fsize)


class WholeFilePut(DedupWorker):

    def __init__(self, work_queue, result_queue, container):
        DedupWorker.__init__(self, work_queue, result_queue, container)

    def do_job(self, job):
        i, obj_name, src_path, authtoken = job
        t = time.time()
        (manifest, hashtime, fsize) = self.put_file(src_path, obj_name)
        data = time.time() - t
        status = self.conn.getinfo(pycurl.RESPONSE_CODE)
        t = time.time()
        self.put_manifest(obj_name, manifest, authtoken)
        meta = time.time() - t
        return (i, data, fsize, meta, len(manifest), hashtime, status == 200)

    def put_file(self, src_path, obj_name):
        t = time.time()
        obj_hash = self.calculate_objid_hash(obj_name)
        hashtime = time.time()-t
        headers = []
        headers.append("X-Chunk-Obj-Id: " + obj_hash)

        with open(src_path) as f:
            metadata = {}
            metadata['_id'] = obj_hash
            fsize = os.fstat(f.fileno()).st_size
            metadata['size'] = fsize
            metadata['chunk_len'] = fsize

            chunk = f.read()
            t = time.time()
            fingerprint = self.calculate_fingerprint(chunk)
            hashtime += time.time()-t
            url = self.chunk_url(fingerprint)
            self.http_put(url, chunk, fsize, headers)

        chunks = []
        chunk_meta = {}
        chunk_meta['id'] = fingerprint
        chunks.append(chunk_meta)
        metadata['chunks'] = chunks

        return (json.dumps(metadata), hashtime, fsize)

            #if fsize == 0:
            #    fingerprint = 'da39a3ee5e6b4b0d3255bfef95601890afd80709'
            #    url = self.chunk_url(fingerprint)
            #    self.http_req(url, "PUT", self.conn_chunk, None, 0, obj_hash=obj_hash)
            #    chunk_meta = {}
            #    chunk_meta['id'] = fingerprint
            #    chunks.append(chunk_meta)


class NonDedupGet(Worker):

    def __init__(self, work_queue, result_queue, container):
        Worker.__init__(self, work_queue, result_queue, container)

    def do_job(self, job):
        i, obj_name, dst_path, authtoken = job
        t = time.time()
        url = self.obj_url(obj_name)
        #print "Getting object: " + url
        with tempfile.TemporaryFile() as f:
            self.http_get(url, [], f.write, authtoken)
        t = time.time() - t
        size_download = self.conn.getinfo(pycurl.SIZE_DOWNLOAD)
        return (i, t, size_download)


class InlineWholeFilePut(Worker):

    def __init__(self, work_queue, result_queue, container):
        Worker.__init__(self, work_queue, result_queue, container)


class WholeFileGet(DedupWorker):

    def __init__(self, work_queue, result_queue, container):
        DedupWorker.__init__(self, work_queue, result_queue, container)

    def do_job(self, job):
        i, obj_name, dst_path, authtoken = job
        t = time.time()
        (manif, len_manif) = self.get_manifest(obj_name, authtoken)
        meta = time.time() - t
        t = time.time()
        self.get_chunks(manif, dst_path)
        data = time.time() - t
        return (i, data, manif['size'], meta, len_manif)
    
