#!/bin/bash

OBJ_NAME=test
INPUT=/home/pgomes/datasets/vm/VDI/ubuntu-8.10-server-x86.vdi
OP=put
NODES=16

NODES_FILE=nodes-16

LOGS=/home/pgomes/logs

TIME_FILE=time

function echo {
  /bin/echo [`date`] $*
}

clean(){
    echo cleaning..
    #stopall $NODES_FILE > /dev/null 2>&1
    cleanall $NODES_FILE > /dev/null 2>&1
    startall $NODES_FILE > /dev/null 2>&1
    ./refresh-token.sh
    echo finished
}

start_monitoring(){
    echo will start monitors..
    MONITOR_LOG=$1
    ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh $LOGS/cpu/$MONITOR_LOG.cpu $LOGS/mem/$MONITOR_LOG.mem 2 swift
    all.sh $NODES_FILE ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh $LOGS/$MONITOR_LOG.cpu $LOGS/$MONITOR_LOG.mem 2 swift
    echo monitors started
}

stop_monitoring(){
    echo "will stop monitoring"
    ~/scripts/stop-monitoring.sh
    all.sh $NODES_FILE ~/scripts/stop-monitoring.sh
    echo "stopped monitoring"
}

PREV=`pwd`

cd /home/pgomes/swift/swift-client

    for np in 32 64
    do
        for block in 66560
        do
            clean
            log=vm-$np-putnew-$block
            /usr/bin/time -p -o $TIME_FILE ./newclient.py -b $block --direct -np $np put $OBJ_NAME $INPUT
            head -1 $TIME_FILE | awk '{printf $2","}' >> $LOGS/$log
            log=vm-$np-putrep-$block
            /usr/bin/time -p -o $TIME_FILE ./newclient.py -b $block --direct -np $np put $OBJ_NAME $INPUT
            head -1 $TIME_FILE | awk '{printf $2","}' >> $LOGS/$log
            log=vm-$np-get-$block
            /usr/bin/time -p -o $TIME_FILE ./newclient.py -b $block --direct -np $np get $OBJ_NAME ./fuck
            head -1 $TIME_FILE | awk '{printf $2","}' >> $LOGS/$log
            rm -rf ./fuck
        done
    done

stopall $NODES_FILE

cd $PREV
