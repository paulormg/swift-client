#!/bin/bash

OBJ_NAME=test
INPUT=./input100M
OP=put
NODES=16
FRESH=true

NODES_FILE=nodes-16

LOGS=/home/pgomes/logs

TYPE=$1
BLOCK_SIZE=512000

TIME_FILE=time

function echo {
  /bin/echo [`date`] $*
}

clean(){
    echo cleaning..
    stopall $NODES_FILE > /dev/null 2>&1
    cleanall $NODES_FILE > /dev/null 2>&1
    startall $NODES_FILE > /dev/null 2>&1
    ./refresh-token.sh
    echo finished
}

start_monitoring(){
    echo will start monitors..
    MONITOR_LOG=$1
    ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh $LOGS/cpu/$MONITOR_LOG.cpu $LOGS/mem/$MONITOR_LOG.mem 2 swift
    all.sh $NODES_FILE ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh $LOGS/$MONITOR_LOG.cpu $LOGS/$MONITOR_LOG.mem 2 swift
    echo monitors started
}

stop_monitoring(){
    echo "will stop monitoring"
    ~/scripts/stop-monitoring.sh
    all.sh $NODES_FILE ~/scripts/stop-monitoring.sh
    echo "stopped monitoring"
}

go(){
    op=$1
    input=$2
    log=replevel-$TYPE-$op-$NP
    #start_monitoring $log
    echo "will start: " $log
    /usr/bin/time -p -o $TIME_FILE ./newclient.py --no-dedup $op $OBJ_NAME $input
    head -1 $TIME_FILE | awk '{printf $2","}' >> $LOGS/$log
    #stop_monitoring
}


PREV=`pwd`

cd /home/pgomes/swift/swift-client

for turn in 1 2 3 
do
    clean
    for op in put get
    do
        if [ "$op" == "put" ]; then
            go $op $INPUT
        else
            go $op ./output_123
            rm -rf ./output_123
        fi
   done
done

stopall $NODES_FILE

cd $PREV
