#!/bin/bash

HOME_DIR=/home/pgomes
CLIENT_DIR=/home/pgomes/swift/swift-client

all.sh clean-all
#
swift-init proxy stop 2>&1 > /dev/null
all.sh swift-init main stop 2>&1 > /dev/null
sleep 3

swift-init proxy start 2>&1 > /dev/null
all.sh swift-init main start 2>&1 > /dev/null
sleep 3

$CLIENT_DIR/cern-client.py put $CLIENT_DIR/catalogs/16.6.0.pkl $HOME_DIR/datasets/cern/files

swift-init proxy stop 2>&1 > /dev/null
all.sh swift-init main stop 2>&1 > /dev/null
sleep 3
swift-init proxy start 2>&1 > /dev/null
all.sh swift-init main start 2>&1 > /dev/null
sleep 3

$CLIENT_DIR/cern-client.py put $CLIENT_DIR/catalogs/16.6.1.pkl $HOME_DIR/datasets/cern/files

swift-init proxy stop 2>&1 > /dev/null
all.sh swift-init main stop 2>&1 > /dev/null
sleep 3

echo "Finished uploading dedup part"

all.sh clean-all

swift-init proxy start 2>&1 > /dev/null
all.sh swift-init main start 2>&1 > /dev/null
sleep 3

$CLIENT_DIR/cern-client.py put $CLIENT_DIR/catalogs/16.6.0.pkl $HOME_DIR/datasets/cern/files

swift-init proxy stop 2>&1 > /dev/null
all.sh swift-init main stop 2>&1 > /dev/null
sleep 3
swift-init proxy start 2>&1 > /dev/null
all.sh swift-init main start 2>&1 > /dev/null
sleep 3

$CLIENT_DIR/cern-client.py put $CLIENT_DIR/catalogs/16.6.1.pkl $HOME_DIR/datasets/cern/files

swift-init proxy stop 2>&1 > /dev/null
all.sh swift-init main stop 2>&1 > /dev/null
sleep 3

echo "Finished uploading dedup2 part"

all.sh clean-all
