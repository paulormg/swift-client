#!/bin/bash

# comecando com 16 nodes

#cd /home/pgomes/swift/swift-client
#
remakerings-16; distributerings nodes-16
#
#echo dedup vs fresh
#
#echo replevel rep2
#
#./dedup_vs_fresh.sh true fresh
#./dedup_vs_fresh.sh false dup

echo nodesclients 16

cd /home/pgomes/swift/swift-client
./nodesclients.sh 16

echo nodesclients 2

remakerings-2; distributerings nodes-16
cd /home/pgomes/swift/swift-client
./nodesclients.sh 2

echo nodesclients4

remakerings-4; distributerings nodes-16
cd /home/pgomes/swift/swift-client
./nodesclients.sh 4

echo nodesclients8

remakerings-8; distributerings nodes-16
cd /home/pgomes/swift/swift-client
./nodesclients.sh 8


#echo throughput srv
#
#./throughput.sh srv
#
#mv /home/pgomes/system/etc/swift/proxy-server.conf /home/pgomes/system/etc/swift/proxytemp.conf
#mv /home/pgomes/system/etc/swift/juru.conf /home/pgomes/system/etc/swift/proxy-server.conf
#
#echo replevel rep2
#
#cd /home/pgomes/swift/swift-client
#
#./replevel.sh rep2
#./replevelwhole.sh whole-rep2
#
#cd /home/pgomes/swift/swift-client
#
#remakerings-16-rep3; distributerings nodes-16
#cd /home/pgomes/swift/swift-client
#
#echo replevel rep3
#
#./replevel.sh rep3
#./replevelwhole.sh whole-rep3
#
#remakerings-16-rep4; distributerings nodes-16
#cd /home/pgomes/swift/swift-client
#
#echo replevel rep4
#
#./replevel.sh rep4
#./replevelwhole.sh whole-rep4
#
##echo back to 16 and dedup vs fresh
#
#remakerings-16; distributerings nodes-16
#
#echo throughput nde

#./throughput.sh nde-direct

#echo now change proxy server conf
#
