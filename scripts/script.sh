#!/bin/bash

HOME_DIR=/home/pgomes
CLIENT_DIR=/home/pgomes/swift/swift-client
NP=4

function echo {
  /bin/echo [`date`] $*
}

clean(){
    echo "Cleaning all.."
    all.sh "clean-all > /dev/null 2>&1"
}

stop_all(){
    swift-init proxy stop 2>&1 > /dev/null
    all.sh "swift-init main stop > /dev/null 2>&1"
}

start_all(){
    swift-init proxy start 2>&1 > /dev/null
    all.sh "swift-init main start > /dev/null 2>&1"
}

restart_all(){
    echo "Restarting all.."
    stop_all
    sleep 3
    start_all
    sleep 3
}

go(){
    VERSION=$1
    ACTION=$2
    OPT=$3
    restart_all
    echo "Starting monitors on all nodes"
    all.sh ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh ~/logs/cern-cpu-$ACTION-$VERSION$OPT.log ~/logs/cern-mem-$ACTION-$VERSION$OPT.log 3 swift
    echo "Starting "$ACTION" of release $VERSION (OPTS:"$OPT")"
    time -p $CLIENT_DIR/newcernclient.py $ACTION $CLIENT_DIR/catalogs/$VERSION.pkl $HOME_DIR/datasets/cern/files -np $NP $OPT
    echo "Finishing "$ACTION" of release $VERSION (OPTS:"$OPT")"
    all.sh ~/scripts/stop-monitoring.sh
}

seq(){
    OPT=$1
    clean
    go 16.6.0 put $OPT
    go 16.6.1 put $OPT
    go 16.6.2 put $OPT
    go 16.6.3 put $OPT
    go 16.6.0 get $OPT
    go 16.6.1 get $OPT
    go 16.6.2 get $OPT
    go 16.6.3 get $OPT
    stop_all
    echo "Finished all releases. Collecting space utilization"
    #all.sh "du -s /home/pgomes/system/mnt/sdb2/objects | awk {'print \$1'}" > /home/pgomes/logs/cern-obj-all$OPT.log
    #if [ ! $OPT ]; then
    #    all.sh "du -s /home/pgomes/system/mnt/sdb1/chunks | awk {'print \$1'}" > /home/pgomes/logs/cern-chunk-all$OPT.log
    #fi
    #echo "Finished collecting space utilization."
}

time seq
sleep 5
time seq --no-dedup
