AUTH_TOKEN=`cat .authtoken`
STORAGE_URL=`cat .storageurl`
LOCAL_FILE=$1
OBJ_NAME=$2

curl -v -X PUT -T $LOCAL_FILE \
    -H "X-Auth-Token: $AUTH_TOKEN" \
    $STORAGE_URL/$OBJ_NAME


#-H "Transfer-encoding: chunked" \
#    -H "Content-Type: application/octet-stream" \
#    -H "X-Object-Meta-Description: Dummy description" \
#    -H "ETag: 805120ec285a7ed28f74024422fe3594" \

#curl -v -X PUT -H "X-Auth-Token: $AUTH_TOKEN" $STORAGE_URL/$CONTAINER
