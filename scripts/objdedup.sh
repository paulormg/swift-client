#!/bin/bash

BLOCK_SIZE=65536
OBJ_NAME=test

NODES_FILE=nodes-16

LOGS=/home/pgomes/logs

FRESH=$1
TYPE=$2
OP=$3
OPT=$4

TIME_FILE=time

function echo {
  /bin/echo [`date`] $*
}

clean(){
    echo cleaning..
    #stopall $NODES_FILE > /dev/null 2>&1
    cleanall $NODES_FILE > /dev/null 2>&1
    startall $NODES_FILE > /dev/null 2>&1
    ./refresh-token.sh
    echo finished
}

go(){
    remote=$1
    local=$2
    log=objdedup-$TYPE-$OP-$remote
    /usr/bin/time -p -o $TIME_FILE ./newclient.py --whole-object $OP $remote $local $OPT
    head -1 $TIME_FILE | awk '{printf $2","}' >> $LOGS/$log
}


PREV=`pwd`

cd /home/pgomes/swift/swift-client

for turn in 1
do
    if [ $FRESH == true ] && [ "$OP" == "put" ]; then
        clean
    else
        startall $NODES_FILE
        ./refresh-token.sh
    fi
    for input in 1K 10K 25K 50K 100K 200K 750K 1M 10M 25M 50M 75M 100M
    do
        if [ "$OP" == "put" ]; then
            go $input ./input$input
        else
            go $input ./output123
            rm -rf ./output123
        fi
    done
    sleep 3
done

stopall $NODES_FILE

cd $PREV
