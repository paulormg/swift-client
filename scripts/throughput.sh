#!/bin/bash

BLOCK_SIZE=65536
OBJ_NAME=test
INPUT=./input100M
OP=put
NODES=16
FRESH=true

NODES_FILE=nodes-16

LOGS=/home/pgomes/logs

TYPE=$1

TIME_FILE=time

function echo {
  /bin/echo [`date`] $*
}

clean(){
    echo cleaning..
    stopall $NODES_FILE > /dev/null 2>&1
    cleanall $NODES_FILE > /dev/null 2>&1
    startall $NODES_FILE > /dev/null 2>&1
    ./refresh-token.sh
    echo finished
}

start_monitoring(){
    echo will start monitors..
    MONITOR_LOG=$1
    ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh $LOGS/cpu/$MONITOR_LOG.cpu $LOGS/mem/$MONITOR_LOG.mem 2 swift
    all.sh $NODES_FILE ~/scripts/background-nolog.sh ~/scripts/start-monitoring.sh $LOGS/$MONITOR_LOG.cpu $LOGS/$MONITOR_LOG.mem 2 swift
    echo monitors started
}

stop_monitoring(){
    echo "will stop monitoring"
    ~/scripts/stop-monitoring.sh
    all.sh $NODES_FILE ~/scripts/stop-monitoring.sh
    echo "stopped monitoring"
}

PREV=`pwd`

cd /home/pgomes/swift/swift-client

for turn in 1
do
    for np in 1 2 4 6 8 10 12
    do
        if $FRESH; then
            clean
        fi
        log=rep-$TYPE-$OP-$np
        #start_monitoring $log
        echo "will start: " $log
        /usr/bin/time -p -o $TIME_FILE ./newclient.py -b $BLOCK_SIZE -np $np put $OBJ_NAME $INPUT
        head -1 $TIME_FILE | awk '{printf $2","}' >> $LOGS/$log
        #stop_monitoring
    done
done

stopall $NODES_FILE

cd $PREV
