#!/usr/bin/python
import os
import sys
import hashlib
import binascii

MEGA=1048576

db = {}

def get_zero_block_hash(block_size):
    empty4kblock_str = ''.join(['00' for num in range(block_size)])
    empty4kblock = binascii.unhexlify(empty4kblock_str)
    m = hashlib.sha1()
    m.update(empty4kblock)
    return  m.digest()

def main():
    path = sys.argv[1]
    block_size = int(sys.argv[2])
    dirs = os.listdir(path)
    dirs = sorted(dirs)
    zero_block = get_zero_block_hash(block_size)
    for fname in dirs:
        print "Dedup statistics for: %s" % (fname)
        with open(path + "/" + fname) as f:
            total = 0
            unique = 0
            blank = 0
            for chunk in iter(lambda: f.read(block_size), ''):
                fingerprint = hashlib.sha1(chunk).digest()
                if fingerprint not in db:
                    db[fingerprint] = None
                    unique +=1
                elif fingerprint == zero_block:
                    blank = 0
                total += 1
        dedup = total-unique
        overhead = 40*(total + dedup) # bytes
        print "Number of blocks: %d. Overhead: %.2f%%. Total volume: %dMB. Duplicated: %dMB (%.2f%%)." \
              "Unique: %dMB (%.2f%%). Zero blocks: %dMB (%.2f%%)" % \
              (total, (overhead*100.0)/(total*block_size), total*block_size/MEGA,
               dedup*block_size/MEGA, dedup*100.0/total, 
               unique*block_size/MEGA, unique*100.0/total,
               blank*block_size/MEGA*block_size, blank*100.0/total)

if __name__ == '__main__':
    main()
