#!/usr/bin/python

import StringIO
import argparse
import cStringIO
import datetime
import hashlib
import json
import multiprocessing
import os
import pickle
import pprint
import pycurl
import sys
import time
import urllib
import workers
import math

# DEFAULT CONFIG VALUES

SERVER_URL="127.0.0.1:8080"
CHUNK_BASE='/chunk/v1/a/c/o/'
OBJ_BASE='/v1/AUTH_test/'

# CONSTANTS

PARAM_ACTION='action'
PARAM_OBJNAME='obj_name'
PARAM_LOCALPATH='local_path'
PARAM_BLOCKSIZE='block_size'
PARAM_DEDUP='dedup'
PARAM_INLINE_SERVER='server-inline'
PARAM_WHOLE_OBJECT='whole-object'
PARAM_DIRECT='direct'
PARAM_NP = 'np'

ACTION_PUT='put'
ACTION_GET='get'

def dummy_write(data):
    #print data
    pass

def parse_args():
    parser = argparse.ArgumentParser(description='Openstack Object Storage Client')
    parser.add_argument(PARAM_ACTION, type=str, choices=[ACTION_PUT, ACTION_GET], 
                        help='the action to execute')
    parser.add_argument(PARAM_OBJNAME, type=str, help='the name of the object')
    parser.add_argument(PARAM_LOCALPATH, type=str, help='location of object in the local machine')
    parser.add_argument('-b', dest=PARAM_BLOCKSIZE, type=int, action='store',
                        default=4096, help='Block Size (default 4096)')
    parser.add_argument('--no-dedup', dest='dedup', action='store_false',
                        help='Do not chunk objects and perform deduplication')
    parser.add_argument('--inline-server', dest=PARAM_INLINE_SERVER, action='store_true',
                        help='Perform fingerprinting at server')
    parser.add_argument('--whole-object', dest=PARAM_WHOLE_OBJECT, action='store_true',
                        help='Do not chunk object, upload it as a single chunk')
    parser.add_argument('--direct', dest=PARAM_DIRECT, action='store_true',
                        help='Do not chunk object, upload it as a single chunk')
    parser.add_argument('-np',dest=PARAM_NP, default=1, type=int, help='num procs')
    args = parser.parse_args()

    return vars(args)

def main(params):
    local_path = params[PARAM_LOCALPATH]
    obj_name = params[PARAM_OBJNAME]

    client = Client('paulo', params)
    client.go(obj_name, local_path)
    #client.close_conn()
    #client.print_stats()

class Client:

    def __init__(self, container, params):
        self.container = container
        self.objbase = SERVER_URL + OBJ_BASE + container + '/'
        self.times = {}
        self.counts = {}
        self.np = params[PARAM_NP]
        self.conn_info = {}
        self.chunk_conn_info = {}
        self.manifest_conn_info = {}
        try:
            self.chunk_size = params[PARAM_BLOCKSIZE]
        except:
            self.chunk_size = None
        self.whole_obj = params[PARAM_WHOLE_OBJECT]
        self.direct = params[PARAM_DIRECT]
        self.inline = params[PARAM_INLINE_SERVER]
        self.dedup = params[PARAM_DEDUP]
        self.action_name = params[PARAM_ACTION]
        if self.action_name == ACTION_PUT:
            self.action = self.put
        elif self.action_name == ACTION_GET:
            self.action = self.get
        self.conn_manif = pycurl.Curl()
        if self.np == 1:
            self.conn_chunk = pycurl.Curl()
 
    def print_stats(self):
        pp = pprint.PrettyPrinter()
        print "Counts:"
        pp.pprint(self.counts)
        print "Times:"
        pp.pprint(self.times)
        if self.dedup:
            print "Chunk info:"
            pp.pprint(self.get_avgs(self.chunk_conn_info))
            print "Manifest info:"
            pp.pprint(self.get_avgs(self.manifest_conn_info))
            #pp.pprint(self.manifest_conn_info['bla'])
        else:
            pp.pprint(self.get_avgs(self.conn_info))
            #pp.pprint(self.conn_info['bla'])

    def get_avgs(self, info):
        total_time = sum(info['total_times'])
        avg_times = sum(info['total_times'])/len(info['total_times'])
        uploaded_size = sum(info['size_uploads'])
        speed = [u for u in info['speed_uploads'] if u != 0]
        avg_speed = 0
        if speed:
            avg_speed = sum(speed)/len(speed)

        new_info = {}
        new_info['num_reqs'] = len(info['total_times'])
        new_info['total_time'] = total_time
        new_info['total_upload'] = uploaded_size
        new_info['avg_times'] = avg_times
        new_info['avg_speed'] = avg_speed

        return new_info

    def close_conn(self):
        if self.conn_chunk:
            self.conn_chunk.close()
            self.conn_chunk = None
        if self.conn_manif:
            self.conn_manif.close()
            self.conn_manif = None
 
    def go(self, objname, lpath):
        self.action(objname, lpath)

    def put(self, obj_name, src_path):
        self.validate_put(src_path)
        if self.dedup:
            self.dedup_put(obj_name, src_path)
        else:
            self.simple_put(obj_name, src_path)

    def get(self, obj_name, dst_path):
        self.validate_get(dst_path)
        if self.dedup:
            self.dedup_get(obj_name, dst_path)
        else:
            self.simple_get(obj_name, dst_path)

    def timer(func):
        def wrapper(*arg, **kwargs):
            self = arg[0]
            t1 = time.time()
            res = func(*arg, **kwargs)
            t2 = time.time()
            thistime = t2 - t1
            try:
                functime = self.times[func.func_name]
                self.times[func.func_name] = functime + thistime
                self.counts[func.func_name] += 1
            except KeyError:
                self.times[func.func_name] = thistime
                self.counts[func.func_name] = 1
            return res
        return wrapper

    def parallel_put(self, obj_name, src_path):
        work_queue = multiprocessing.JoinableQueue(5000)
        result_queue = multiprocessing.JoinableQueue()

        wrks = []
        # spawn workers
        for i in range(self.np):
            worker = workers.NonDedupPut(work_queue, result_queue, self.container)
            worker.start()
            wrks.append(worker)

        for i in range(self.np):
            job = (i, obj_name + str(i), src_path, self.get_token())
            work_queue.put(job)

        work_queue.join()
        for worker in wrks:
            worker.terminate()

    #@timer
    def simple_put(self, obj_name, src_path):
        url = self.obj_url(obj_name)
        auth_token = self.get_token()
        with open(src_path) as f:
            fsize = os.fstat(f.fileno()).st_size
            self.http_req(url, 'PUT', self.conn_chunk, f, fsize,
                     auth_token=auth_token, fn=src_path)

    #@timer
    def simple_get(self, obj_name, dst_path):
        auth_token = self.get_token()
        url = self.obj_url(obj_name)
        #print "Getting object: " + url
        with open(dst_path, 'w') as f:
            self.http_req(url, 'GET', self.conn_chunk, write_function=f.write,
                    auth_token=auth_token, fn=dst_path)
        #print "File succesfully written to: " + dst_path

    def get_token(self):
        try:
            with open(".authtoken") as f:
                return f.read().strip()
        except Exception:
            sys.exit("ERROR: Please run ./refresh-token.sh before using"
                     " the client.")

    def validate_put(self, src_path):
        if not os.path.exists(src_path):
            sys.exit("ERROR: File '%s' does not exist" % src_path)

    def validate_get(self, dst_path):
        if not os.path.isdir(os.path.dirname(dst_path)):
            sys.exit("ERROR: '%s' directory must be created" % dst_path)

    #@timer
    def dedup_get(self, obj_name, dst_path):
        manifest = self.get_manifest(obj_name)
        if self.np > 1:
            self.parallel_get_chunks(manifest, dst_path)
        else:
            self.get_chunks(manifest, dst_path)

    #@timer
    def get_manifest(self, obj_name):
        self.conn_info = self.manifest_conn_info

        url = self.obj_url(obj_name)
        auth_token = self.get_token()
        buf = StringIO.StringIO()
        self.http_req(url, 'GET', self.conn_manif, auth_token=auth_token, 
                 write_function=buf.write)

        manifest = buf.getvalue()
        return dict(json.loads(manifest))

    #@timer
    def get_chunks(self, manifest, dst_path):
        self.conn_info = self.chunk_conn_info

        #print "Getting chunks"
        chunks = manifest['chunks']
        with open(dst_path, 'w') as f:
            for chunk in chunks:
                fingerprint = chunk['id'].strip().encode('utf-8')
                url = self.chunk_url(fingerprint)
                self.http_req(url, 'GET', self.conn_chunk, write_function=f.write, fn=dst_path)
    #print "File succesfully written to: " + dst_path

    #@timer
    def dedup_put(self, obj_name, src_path):
        if self.np > 1:
            manifest = self.parallel_put_chunks(src_path, obj_name)
        else:
            manifest = self.put_chunks(src_path, obj_name)
        self.put_manifest(obj_name, manifest)

    #@timer
    def put_manifest(self, obj_name, manifest):
        self.conn_info = self.manifest_conn_info
        auth_token = self.get_token()
        url = self.obj_url(obj_name)
        manifest = manifest.encode('utf-8')
        self.http_req(url, "PUT", self.conn_manif, manifest, len(manifest),
                 'manifest', auth_token)

    def get_chunk_id(self, buf, line):
        if line.startswith('X-Chunk-Id'):
            buf.write(line.split(':')[1].strip())

    def inline_put(self, src_path, obj_name, obj_hash):

        with open(src_path) as f:
            metadata = {}
            metadata['_id'] = obj_name
            fsize = os.fstat(f.fileno()).st_size
            metadata['size'] = fsize
            if self.whole_obj:
                metadata['chunk_len'] = fsize
            else:
                metadata['chunk_len'] = self.chunk_size
            chunks = None
            if self.whole_obj:
                chunks = self.whole_obj_inline_put(f, fsize, src_path)
            else:
                chunks = self.chunked_inline_put(f, fsize, src_path)
            metadata['chunks'] = chunks

        return json.dumps(metadata)

    def chunked_inline_put(self, f, fsize, src):
        chunks = []
        to_upload = fsize
        while to_upload > 0:
            length = min(to_upload, self.chunk_size)
            url = self.chunk_url('')
            buf = cStringIO.StringIO()
            self.http_req(url, "PUT", self.conn_chunk, f.read, length, header_function=
                          (lambda line: self.get_chunk_id(buf, line)), fn=src)
            chunk_id = buf.getvalue()
            buf.close()
            to_upload -= length
            chunk_meta = {}
            chunk_meta['id'] = chunk_id
            if length != self.chunk_size:
                chunk_meta['len'] = length
            chunks.append(chunk_meta)

        return chunks
 
    #@timer
    def whole_obj_inline_put(self, f, fsize, src):
        url = self.chunk_url('')
        buf = cStringIO.StringIO()
        self.http_req(url, "PUT", self.conn_chunk, f, fsize, header_function=
                (lambda line: self.get_chunk_id(buf, line)), fn=src)
        chunk_id = buf.getvalue()
        buf.close()
        chunk_meta = {}
        chunk_meta['id'] = chunk_id
    
        return [chunk_meta]

    def parallel_get_chunks(self, manifest, dst_path):
        self.conn_info = self.chunk_conn_info

        #print "Getting chunks"
        chunks = manifest['chunks']

        work_queue = multiprocessing.JoinableQueue(5000)
        result_queue = multiprocessing.JoinableQueue()

        wrks = []
        # spawn workers
        for i in range(self.np):
            worker = workers.GetChunkWorker(work_queue, result_queue, self.direct)
            worker.start()
            wrks.append(worker)
        a, b = multiprocessing.Pipe()
        print len(chunks)
        logger_worker = workers.PersistentAndLoggerWorker(result_queue, b, 
                                                      len(chunks), dst_path)
        logger_worker.start()

        i = 0
        for chunk in chunks:
            fingerprint = chunk['id'].strip().encode('utf-8')
            #url = self.chunk_url(fingerprint)
            job = (i, fingerprint)
            work_queue.put(job)
            i += 1
        
        work_queue.join()
        for worker in wrks:
            worker.terminate()
        
        times = a.recv()
        logger_worker.terminate()

    def parallel_put_chunks(self, src_path, obj_name):
        obj_hash=self.calculate_objid_hash(obj_name)

        with open(src_path) as f:
            metadata = {}
            metadata['_id'] = obj_hash
            fsize = os.fstat(f.fileno()).st_size
            metadata['size'] = fsize
            metadata['chunk_len'] = self.chunk_size
            chunks = []

            num_chunks = math.ceil(float(fsize)/self.chunk_size)

            work_queue = multiprocessing.JoinableQueue(1000)
            result_queue = multiprocessing.JoinableQueue()

            wrks = []
            # spawn workers
            for i in range(self.np):
                worker = workers.PutChunkWorker(work_queue, result_queue, obj_hash, 
                                                self.direct)
                worker.start()
                wrks.append(worker)
            a, b = multiprocessing.Pipe()
            logger_worker = workers.ManifestAndLoggerWorker(result_queue, b, num_chunks)
            logger_worker.start()

            i = 0
            for chunk in iter(lambda: f.read(self.chunk_size), ''):
                job = (i,chunk)
                work_queue.put(job)
                i += 1
            
            work_queue.join()
            for worker in wrks:
                worker.terminate()
            bla = a.recv()
            print len(bla)
            chunks, stats = bla
            metadata['chunks'] = chunks
            logger_worker.terminate()
    
        return json.dumps(metadata)

    #@timer
    def put_chunks(self, src_path, obj_name):
        self.conn_info = self.chunk_conn_info

        obj_hash=self.calculate_objid_hash(obj_name)

        if self.inline:
            return self.inline_put(src_path, obj_name, obj_hash)

        with open(src_path) as f:
            metadata = {}
            metadata['_id'] = obj_hash
            fsize = os.fstat(f.fileno()).st_size
            metadata['size'] = fsize
            if self.whole_obj:
                self.chunk_size = fsize
            metadata['chunk_len'] = self.chunk_size
            chunks = []

            if fsize != 0:
                for chunk in iter(lambda: f.read(self.chunk_size), ''):
                    length = len(chunk)
                    fingerprint = self.calculate_fingerprint(chunk)
                    url = self.chunk_url(fingerprint)
                    self.http_req(url, "PUT", self.conn_chunk, 
                                  chunk, length, obj_hash=obj_hash)
                    chunk_meta = {}
                    chunk_meta['id'] = fingerprint
                    if length != self.chunk_size:
                        chunk_meta['len'] = length
                    chunks.append(chunk_meta)
            else:
                fingerprint = 'da39a3ee5e6b4b0d3255bfef95601890afd80709'
                url = self.chunk_url(fingerprint)
                self.http_req(url, "PUT", self.conn_chunk, None, 0, obj_hash=obj_hash)
                chunk_meta = {}
                chunk_meta['id'] = fingerprint
                chunks.append(chunk_meta)
 
            metadata['chunks'] = chunks
    
        return json.dumps(metadata)

    #@timer
    def calculate_fingerprint(self, chunk):
        return hashlib.sha1(chunk).hexdigest()

    #@timer
    def calculate_objid_hash(self, objid):
        return hashlib.md5(objid).hexdigest()

    def obj_url(self, obj_name):
        return self.objbase + obj_name

    def chunk_url(self, fingerprint):
        return SERVER_URL + CHUNK_BASE + fingerprint

    #@timer
    def http_req(self, url, method, conn, data=None, length=None, 
              type='chunk', auth_token=None,
              write_function=None, header_function=None,
              obj_hash=None, fn=None):
        headers = []

        if auth_token:
            headers.append('X-Auth-Token: ' + str(auth_token))

        split = url.split('/',1)
        url = split[0] + '/' + urllib.quote(split[1])

        #conn.setopt(pycurl.VERBOSE, True)
        conn.setopt(pycurl.FAILONERROR, True)
        conn.setopt(pycurl.URL, url)
        conn.setopt(pycurl.NOPROGRESS, 1)

        if obj_hash:
            headers.append("X-Chunk-Obj-Id: " + obj_hash)

        if method == 'PUT':
            headers = self.setup_put(type, data, length, headers, conn)
        elif method == 'GET':
            conn.setopt(pycurl.WRITEFUNCTION, write_function)
        if header_function:
            conn.setopt(pycurl.HEADERFUNCTION, header_function)
        conn.setopt(pycurl.HTTPHEADER, headers)

        self.do_request(url, fn, conn)
        conn.setopt(pycurl.FRESH_CONNECT, 0)
        self.get_stats(conn, url, length)


    #@timer
    def get_stats(self, c, url, length):
        info = self.conn_info
        thistime = str(time.time())
        total_time = c.getinfo(pycurl.TOTAL_TIME)
        #connect_time = c.getinfo(pycurl.CONNECT_TIME)
        #pre_transfer = c.getinfo(pycurl.PRETRANSFER_TIME)
        size_upload = c.getinfo(pycurl.SIZE_UPLOAD)
        speed_upload = c.getinfo(pycurl.SPEED_UPLOAD)
        #clength_upload = c.getinfo(pycurl.CONTENT_LENGTH_UPLOAD)
        status = c.getinfo(pycurl.RESPONSE_CODE)
        

        #if length <= 500 and total_time > 0.5:
        #    self.log('** Request with length %d took %.4fs. Size upload: %d. Speed upload: %d. Status: %d. URL: %s.\n' \
        #          '-- Last length: %d. Last time: %.4fs. Last size: %d. Last speed: %d. Last Status:%d Last URL: %s.' % \
        #          (length, total_time, size_upload, speed_upload, status, url, \
        #           self.lastlen, self.lasttime, self.lastupload, self.lastspeed, self.lastst, self.lasturl))

        #self.lastlen = length
        #self.lasttime = total_time
        #self.lastupload = size_upload
        #self.lastspeed = speed_upload
        #self.lastst = status
        #self.lasturl = url

        if status == 200:
            size_upload = 0
            speed_upload = 0
 

        try:
           #info['bla'].append((total_time, size_upload))
           info['time'].append(thistime)
           info['total_times'].append(total_time)
           #info['connect_times'].append(connect_time)
           #info['pre_transfers'].append(pre_transfer)
           info['size_uploads'].append(size_upload)
           info['speed_uploads'].append(speed_upload)
           #info['clength_upload'].append(clength_upload)
        except KeyError:
           #info['bla'] = [(total_time, size_upload)]
           info['time'] = [thistime]
           info['total_times'] = [total_time]
           #info['connect_times'] = [connect_time]
           #info['pre_transfers'] = [pre_transfer]
           info['size_uploads'] = [size_upload]
           info['speed_uploads'] = [speed_upload]
           #info['clength_upload'] = [clength_upload]
     
    #@timer
    def do_request(self, url, filename, conn):
        i = 0
        while True:
            try:
                conn.perform()
                break
            except Exception, exc:
                conn.setopt(pycurl.FRESH_CONNECT, 1)
                if i < 2:
                    i += 1
                    print "Error while writing chunk of file: %s to object: %s. Retry " \
                          "Count: %d" % (filename, url, i)
                else:
                    print "Error while writing chunk of file: %s to object: %s." \
                          "Already tried %d times. Exiting." % (filename, url, i)
                    raise exc
 
    def setup_put(self, type, data, length, headers, conn):
        if type == 'manifest':
            headers.append("Content-type: manifest/json")
        else:
            headers.append("Content-type: application/octet-stream")
    
        conn.setopt(pycurl.WRITEFUNCTION, dummy_write)
        conn.setopt(pycurl.UPLOAD, 1)
        conn.setopt(pycurl.INFILESIZE, length)
        if not data:
            headers.append('Expect:')
        else:
            if isinstance(data, file):
                conn.setopt(pycurl.INFILE, data)
            elif isinstance(data, str):
                data = cStringIO.StringIO(data)
                conn.setopt(pycurl.READFUNCTION, data.read)
            else:
                conn.setopt(pycurl.READFUNCTION, data)
 
        return headers

    def log(self, msg):
        print "[%s] %s" % (str(datetime.datetime.now()), msg)
        sys.stdout.flush()

    def write_stats(self, stats_file):
        with open(stats_file, "w") as f:
            pickle.dump(self.counts, f)
            pickle.dump(self.times, f)
            if self.dedup:
                pickle.dump(self.chunk_conn_info, f)
                pickle.dump(self.manifest_conn_info, f)
            else:
                pickle.dump(self.conn_info, f)

    def checkpoint(self, stats_file, stats):
        self.write_stats(stats_file, stats)
        self.log("Checkpointed statistics on %s after " \
            "%d files. Downloading next file in %d seconds."
            "(it is safe to stop process now!)" % \
            (stats_file, stats['total_count'], 5))
        time.sleep(5)
        self.log("Back from sleep. (not safe anymore)")

if __name__ == "__main__":

    args = parse_args()
    main(args)

